<?php
/**
 * @file
 * Administration page callbacks for the edgagement plugin.
 */

function edgagement_plugin_settings() {
    $form = array();
    
    $form['edgagement_plugin_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Edgagement Site URL'),
        '#default_value' => variable_get('edgagement_plugin_url', ''),
        '#description' => t('Edgagement Site URL'),
        '#required' => TRUE,
    );
    
    $form['edgagement_plugin_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Edgagement Site Key'),
        '#default_value' => variable_get('edgagement_plugin_key', ''),
        '#description' => t('Edgagement Site Key'),
        '#required' => TRUE,
    );
 
    return system_settings_form($form);
}