(function ($){
    function setOptions(holder){
        var selbox = holder.find('select');
        var infoBox = holder.find('.edgagement_game_infobox');
        $.get('/get/edgagement/gamelist', null, function (data){
            var x, y, options = '';
            for(x in data){
                if($.isPlainObject(data[x])){
                    options += '<optgroup label="'+x+'">';
                    for(y in data[x]){
                        options += '<option value="'+y+'">'+data[x][y]+'</option>';
                    }
                    options += '</optgroup>';
                }else{
                    options += '<option value="'+x+'">'+data[x]+'</option>';
                }
            }
            selbox.html(options);
        });

        selbox.change(function (){
            var el = this;
            if(el.value>0) infoBox.html('Please copy & paste <span class="edgagement_tip">[edgame]'+el.value+'|'+el.options[el.selectedIndex].innerText+'[/edgame]</span> to where you want put the game');
            else infoBox.html('');
        });
    }
    window.edgagement_game_enable = function(el){
        var listHolder = $(el).parent().next();
        if(listHolder.length == 0 || !listHolder.hasClass('edgagement_game_list')){
            listHolder = $('<div class="edgagement_game_list">Please select the game you want put<br><select class="form-select"></select><div class="edgagement_game_infobox"></div></div>').insertAfter($(el).parent());
            setOptions(listHolder);
        }
        if(el.checked){
            listHolder.show();
        }else{
            listHolder.hide();
        }
    }
})(jQuery);
